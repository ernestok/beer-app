import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import BeerApp from '../components/BeerApp'
import BeerItemModal from '../components/BeerItemModal'
import Header from '../components/Header'
import Page404 from '../components/Page404'

const AppRouter = () => (
  <BrowserRouter>
    <div>
      <Header />
      <Switch>
        <Route path="/" exact={true} component={BeerApp} />
        <Route path="/details/:id" component={BeerItemModal} />
        <Route component={Page404} />
      </Switch>
    </div>
  </BrowserRouter>
)

export default AppRouter