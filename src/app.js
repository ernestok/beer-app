import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import AppRouter from './routers/AppRouter'
import BeerApp from './components/BeerApp'
import configureStore from './redux/store/configureStore'
import 'normalize.css/normalize.css'
import './styles/styles.scss'

const store = configureStore()
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppRouter>
          <BeerApp />
        </AppRouter>
      </Provider>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('app'))
