export const GET_DATA_REQUESTED = 'GET_DATA_REQUESTED'
export const GET_BEERS_DONE = 'GET_BEERS_DONE'
export const GET_BEER_BY_ID_DONE = 'GET_BEER_BY_ID_DONE'
export const GET_BY_ABV_DONE = 'GET_BY_ABV_DONE'
export const GET_BY_IBU_DONE = 'GET_BY_IBU_DONE'
export const GET_DATA_FAILED = 'GET_DATA_FAILED'
export const CLEAR_MODAL_BEER = 'CLEAR_MODAL_BEER'

export function getDataRequested() {
  return {
    type: 'GET_DATA_REQUESTED'
  }
}

export function getBeersDone(beers, page) {
  return {
    type: 'GET_BEERS_DONE',
    payload: beers,
    page: page
  }
}

export function getBeerByIdDone(data, beerId) {
  return {
    type: 'GET_BEER_BY_ID_DONE',
    payload: data,
    beerId: beerId
  }
}

export function getByAbvDone(data, abv) {
  return {
    type: 'GET_BY_ABV_DONE',
    payload: data,
    simmilarBeers: abv
  }
}

export function getByIbuDone(data, ibu) {
  return {
    type: 'GET_BY_IBU_DONE',
    payload: data,
    simmilarBeers: ibu
  }
}

export function clearModalBeer() {
  return {
    type: 'CLEAR_MODAL_BEER'
  }
}

export function getDataFailed(error) {
  return {
    type: 'GET_DATA_FAILED',
    payload: error
  }
}

export function getBeers(param) {
  const buildApi = (param) => {
    const API = `https://api.punkapi.com/v2/beers`
    const queryPage = `?page=${param}&per_page=20`

    return API.concat(queryPage)
  }

  return dispatch => {
    dispatch(getDataRequested())

    return fetch(buildApi(param))
      .then(response => response.json())
      .then(data => {
        dispatch(getBeersDone(data, param))
      })
      .catch(error => {
        dispatch(getDataFailed(error))
      })
  }
}

export function getBeerById(param) {
  const buildApi = (param) => {
    const API = `https://api.punkapi.com/v2/beers`
    const queryId = `/${param}`

    return API.concat(queryId)
  }

  return dispatch => {
    dispatch(getDataRequested())

    return fetch(buildApi(param))
      .then(response => response.json())
      .then(data => {
        dispatch(getBeerByIdDone(data, param))
      })
      .catch(error => {
        dispatch(getDataFailed(error))
      })
  }
}

export function getByAbv(param) {
  const buildApi = (param) => {
    const rounded = Math.round(param)
    const API = `https://api.punkapi.com/v2/beers`
    const queryAbv = `/?abv_gt=${rounded}&abv_lt=${rounded + 1}&per_page=3`

    return API.concat(queryAbv)
  }

  return dispatch => {
    dispatch(getDataRequested())

    return fetch(buildApi(param))
      .then(response => response.json())
      .then(data => {
        dispatch(getByAbvDone(data, param))
      })
      .catch(error => {
        dispatch(getDataFailed(error))
      })
  }
}

export function getByIbu(param) {
  const buildApi = (param) => {
    const rounded = Math.round(param)
    const min = rounded <= 10 ? 1 : rounded - 10
    const max = rounded + 20
    const API = `https://api.punkapi.com/v2/beers`
    const queryAbv = `/?ibu_gt=${min}&ibu_lt=${max}&per_page=3`

    return API.concat(queryAbv)
  }

  return dispatch => {
    dispatch(getDataRequested())

    return fetch(buildApi(param))
      .then(response => response.json())
      .then(data => {
        dispatch(getByIbuDone(data, param))
      })
      .catch(error => {
        dispatch(getDataFailed(error))
      })
  }
}

export function getSimmilarAbv(param) {

  return (dispatch, getState) => {

    dispatch(getBeerById(param))
      .then(() => {
        const beerAbv = getState().beer[0].abv
        return dispatch(getByAbv(beerAbv))
      })
      .catch(error => {
        dispatch(getDataFailed(error))
      })
  }
}

export function getSimmilarIbu(param) {

  return (dispatch, getState) => {

    dispatch(getBeerById(param))
      .then(() => {
        const beerIbu = getState().beer[0].ibu
        return dispatch(getByIbu(beerIbu))
      })
      .catch(error => {
        dispatch(getDataFailed(error))
      })
  }
}
