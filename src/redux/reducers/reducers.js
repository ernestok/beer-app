import * as actions from '../actions/actions'

const ReducerDefaultState = {
  isLoading: false,
  isError: false,
  beers: [],
  beer: [],
  page: 0,
  simmilarBeers: []
}

export default (state = ReducerDefaultState, action) => {
  switch (action.type) {
    case actions.GET_DATA_REQUESTED:
      return {
        ...state,
        isLoading: true
      }
    case actions.GET_BEERS_DONE:
      return {
        ...state,
        isLoading: false,
        beers: state.beers.concat(action.payload),
        page: action.page
      }
    case actions.GET_BEER_BY_ID_DONE:
      return {
        ...state,
        isLoading: false,
        beer: action.payload,
        beerId: action.beerId
      }
    case actions.GET_BY_ABV_DONE:
      return {
        ...state,
        isLoading: false,
        simmilarBeers: action.payload
      }
    case actions.GET_BY_IBU_DONE:
      return {
        ...state,
        isLoading: false,
        simmilarBeers: state.simmilarBeers.concat(action.payload)
      }
    case actions.CLEAR_MODAL_BEER:
      return {
        ...state,
        beer: [],
        simmilarBeers: []
      }
    case actions.GET_DATA_FAILED:
      return {
        ...state,
        isLoading: false,
        isError: true
      }
    default:
      return state
  }
}