import { createSelector } from 'reselect'

export const filterSimmilarBeers = (simmilarBeers, particularBeer) => {
  const detalisArr = simmilarBeers.map((beer) => {
    return {
      id: beer.id,
      name: beer.name,
      image_url: beer.image_url,
    }
  })
  const noParticularBeer = detalisArr.filter(item => item.id !== particularBeer[0].id)
  const noDuplicates = noParticularBeer.beer = noParticularBeer.filter((beer, index, self) =>
    index === self.findIndex((otherBeer) => (
      otherBeer.place === beer.place && otherBeer.name === beer.name
    ))
  )

  return noDuplicates.slice(0, 3)
}

export const getNextPage = (state) => state.page

export const isError = (state) => state.isError

export const isLoading = (state) => state.isLoading

export const ListedBeers = (state) => state.beers.map((beer) => {
  return {
    id: beer.id,
    name: beer.name,
    tagline: beer.tagline,
    image_url: beer.image_url
  }
})

const getBeer = (state) => state.beer.map((beer) => {
  return {
    id: beer.id,
    name: beer.name,
    tagline: beer.tagline,
    image_url: beer.image_url,
    description: beer.description,
    brewers_tips: beer.brewers_tips,
    ibu: beer.ibu,
    abv: beer.abv
  }
})[0]

export const getBeerState = createSelector(
  [getBeer],
  (bar) => bar
)
