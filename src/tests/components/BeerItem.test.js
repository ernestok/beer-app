import React from 'react'
import { shallow } from 'enzyme'
import BeerItem from '../../components/BeerItem'
import { beer } from '../fixtures/beer.js'

let wrapper

describe('<BeerItem /> default', () => {
  beforeEach(() => {
    wrapper = shallow(<BeerItem
      beer={beer}
    />)
  })

  it('should render BeerItem snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
