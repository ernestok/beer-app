const beersListMock = [{
  id: 1,
  image_url: 'image_url',
  name: 'Zywiec Bialy',
  tagline: 'Z Zywcem robota idzie lekko',
  abv: 1,
  ibu: 23
},
{
  id: 2,
  image_url: 'image_url',
  name: 'Tyskie',
  tagline: 'Ze Śląska',
  abv: 7,
  ibu: 20
},
{
  id: 8,
  image_url: 'image_url',
  name: 'Grolsh',
  tagline: 'Close it!',
  abv: 7,
  ibu: 17
}]

const duplicatedBeers = [{
  id: 1,
  image_url: 'image_url',
  name: 'Zywiec Bialy',
  tagline: 'Z Zywcem robota idzie lekko',
  abv: 1,
  ibu: 23
},
{
  id: 2,
  image_url: 'image_url',
  name: 'Tyskie',
  tagline: 'Ze Śląska',
  abv: 7,
  ibu: 20
},
{
  id: 1,
  image_url: 'image_url',
  name: 'Zywiec Bialy',
  tagline: 'Z Zywcem robota idzie lekko',
  abv: 1,
  ibu: 23
},
{
  id: 2,
  image_url: 'image_url',
  name: 'Tyskie',
  tagline: 'Ze Śląska',
  abv: 7,
  ibu: 20
},
{
  id: 8,
  image_url: 'image_url',
  name: 'Grolsh',
  tagline: 'Close it!',
  abv: 7,
  ibu: 17
},
{
  id: 9,
  image_url: 'image_url',
  name: 'Warka',
  tagline: 'Walcz!',
  abv: 7,
  ibu: 16
},
{
  id: 10,
  image_url: 'image_url',
  name: 'Lech',
  tagline: 'Zielony uspokaja',
  abv: 7,
  ibu: 16
}]

const particularBeer = [{
  id: 9,
  image_url: 'image_url',
  name: 'Warka',
  tagline: 'Walcz!',
  abv: 7,
  ibu: 16
}]

const filteredNoDuplicates = [{
  id: 1,
  image_url: 'image_url',
  name: 'Zywiec Bialy',
},
{
  id: 2,
  image_url: 'image_url',
  name: 'Tyskie',
},
{
  id: 8,
  image_url: 'image_url',
  name: 'Grolsh',
}]

export {
  beersListMock,
  duplicatedBeers,
  particularBeer,
  filteredNoDuplicates
}