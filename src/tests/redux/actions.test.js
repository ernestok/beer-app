import * as actions from '../../redux/actions/actions'
import * as constants from '../fixtures/reduxActions'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'

describe('Single action creators', () => {
  it('should create an action to get data requested', () => {
    const expectedAction = {
      type: actions.GET_DATA_REQUESTED,
    }
    expect(actions.getDataRequested()).toEqual(expectedAction)
  })

  it('should create an action to fetch beers per page', () => {
    const payload = constants.beersListMock
    const page = 7
    const expectedAction = {
      type: actions.GET_BEERS_DONE,
      payload,
      page
    }
    expect(actions.getBeersDone(payload, page)).toEqual(expectedAction)
  })

  it('should create an action to fetch beer by id', () => {
    const payload = constants.beersListMock[0]
    const beerId = 2
    const expectedAction = {
      type: actions.GET_BEER_BY_ID_DONE,
      payload,
      beerId
    }
    expect(actions.getBeerByIdDone(payload, beerId)).toEqual(expectedAction)
  })

  it('should create an action to fetch beer by abv', () => {
    const payload = constants.beersListMock
    const abv = 8
    const expectedAction = {
      type: actions.GET_BY_ABV_DONE,
      payload,
      simmilarBeers: abv
    }
    expect(actions.getByAbvDone(payload, abv)).toEqual(expectedAction)
  })

  it('should create an action to fetch beer by ibu', () => {
    const payload = constants.beersListMock
    const ibu = 9
    const expectedAction = {
      type: actions.GET_BY_IBU_DONE,
      payload,
      simmilarBeers: ibu
    }
    expect(actions.getByIbuDone(payload, ibu)).toEqual(expectedAction)
  })

  it('should create an action to clear modal beer', () => {
    const expectedAction = {
      type: actions.CLEAR_MODAL_BEER,
    }
    expect(actions.clearModalBeer()).toEqual(expectedAction)
  })

  it('should create an action to clear modal beer', () => {
    const error = 'warn, sth went wrong'
    const expectedAction = {
      type: actions.GET_DATA_FAILED,
      payload: error
    }
    expect(actions.getDataFailed(error)).toEqual(expectedAction)
  })
})

describe('Actions with thunk middleware', () => {
  const middlewares = [thunk]
  const mockStore = configureMockStore(middlewares)

  afterEach(() => {
    fetchMock.restore()
  })

  it('creates GET_BEERS_DONE as success when fetching beers has been done', () => {
    const page = 1
    const mockAPI = `https://api.punkapi.com/v2/beers?page=${page}&per_page=20`

    fetchMock.mock(mockAPI, {
      beers: constants.beersListMock,
    })

    const expectedActions = [
      { type: actions.GET_DATA_REQUESTED },
      {
        type: actions.GET_BEERS_DONE, payload: {
          beers: constants.beersListMock,
        },
        page
      }
    ]
    const store = mockStore({ reducers: [] })

    return store.dispatch(actions.getBeers(page)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates GET_BEER_BY_ID_DONE as success when fetching beers by id has been done', () => {
    const queryId = 2
    const mockAPI = `https://api.punkapi.com/v2/beers/${queryId}`

    fetchMock.mock(mockAPI, {
      beers: constants.beersListMock[queryId],
    })

    const expectedActions = [
      { type: actions.GET_DATA_REQUESTED },
      {
        type: actions.GET_BEER_BY_ID_DONE, payload: {
          beers: constants.beersListMock[queryId],
        },
        beerId: queryId
      }
    ]
    const store = mockStore({ reducers: [] })

    return store.dispatch(actions.getBeerById(queryId)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates GET_BY_ABV_DONE as success when fetching beers by abv has been done', () => {
    const mockRounded = 19
    const mockAPI = `https://api.punkapi.com/v2/beers/?abv_gt=${mockRounded}&abv_lt=${mockRounded + 1}&per_page=3`

    fetchMock.mock(mockAPI, {
      beers: constants.beersListMock,
    })

    const expectedActions = [
      { type: actions.GET_DATA_REQUESTED },
      {
        type: actions.GET_BY_ABV_DONE, payload: {
          beers: constants.beersListMock,
        },
        simmilarBeers: mockRounded
      }
    ]
    const store = mockStore({ reducers: [] })

    return store.dispatch(actions.getByAbv(mockRounded)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates GET_BY_IBU_DONE as success when fetching beers by ibu has been done', () => {
    const mockRounded = 19
    const mockAPI = `https://api.punkapi.com/v2/beers/?ibu_gt=${mockRounded - 10}&ibu_lt=${mockRounded + 20}&per_page=3`

    fetchMock.mock(mockAPI, {
      beers: constants.beersListMock,
    })

    const expectedActions = [
      { type: actions.GET_DATA_REQUESTED },
      {
        type: actions.GET_BY_IBU_DONE, payload: {
          beers: constants.beersListMock,
        },
        simmilarBeers: mockRounded
      }
    ]
    const store = mockStore({ reducers: [] })

    return store.dispatch(actions.getByIbu(mockRounded)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})