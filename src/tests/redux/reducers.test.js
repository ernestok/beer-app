import reducer from '../../redux/reducers/reducers'
import * as actions from '../../redux/actions/actions'
import * as constants from '../fixtures/reduxActions'

describe('todos reducer', () => {
  const ReducerExampleState = {
    isLoading: false,
    isError: false,
    beers: [
      { beer: 'Lech' }
    ],
    beer: [],
    page: 0,
    simmilarBeers: [{
      abv: '11',
      ibu: '12'
    }]
  }


  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        isLoading: false,
        isError: false,
        beers: [],
        beer: [],
        page: 0,
        simmilarBeers: []
      }
    )
  })

  it('should handle GET_DATA_REQUESTED', () => {

    expect(
      reducer(ReducerExampleState, {
        type: actions.GET_DATA_REQUESTED
      })
    ).toEqual(
      {
        ...ReducerExampleState,
        isLoading: true
      }
    )
  })

  it('should handle GET_BEERS_DONE', () => {
    expect(
      reducer(ReducerExampleState, {
        type: actions.GET_BEERS_DONE,
        payload: constants.beersListMock,
        page: 3
      })
    ).toEqual(
      {
        ...ReducerExampleState,
        isLoading: false,
        beers: ReducerExampleState.beers.concat(constants.beersListMock),
        page: 3
      }
    )
  })

  it('should handle GET_BEER_BY_ID_DONE', () => {
    expect(
      reducer(ReducerExampleState, {
        type: actions.GET_BEER_BY_ID_DONE,
        payload: constants.beersListMock[0],
        beerId: 4
      })
    ).toEqual(
      {
        ...ReducerExampleState,
        isLoading: false,
        beer: constants.beersListMock[0],
        beerId: 4
      }
    )
  })

  it('should handle GET_BY_ABV_DONE', () => {
    expect(
      reducer(ReducerExampleState, {
        type: actions.GET_BY_ABV_DONE,
        payload: constants.beersListMock,
        simmilarBeers: 17
      })
    ).toEqual(
      {
        ...ReducerExampleState,
        isLoading: false,
        simmilarBeers: constants.beersListMock
      }
    )
  })

  it('should handle GET_BY_IBU_DONE', () => {
    expect(
      reducer(ReducerExampleState, {
        type: actions.GET_BY_IBU_DONE,
        payload: constants.beersListMock,
        simmilarBeers: 17
      })
    ).toEqual(
      {
        ...ReducerExampleState,
        isLoading: false,
        simmilarBeers: ReducerExampleState.simmilarBeers.concat(constants.beersListMock),
      }
    )
  })

  it('should handle CLEAR_MODAL_BEER', () => {
    expect(
      reducer(ReducerExampleState, {
        type: actions.CLEAR_MODAL_BEER
      })
    ).toEqual(
      {
        ...ReducerExampleState,
        beer: [],
        simmilarBeers: [],
      }
    )
  })

  it('should handle GET_DATA_FAILED', () => {
    expect(
      reducer(ReducerExampleState, {
        type: actions.GET_DATA_FAILED,
        payload: 'error occured test'
      })
    ).toEqual(
      {
        ...ReducerExampleState,
        isLoading: false,
        isError: true
      }
    )
  })
})
