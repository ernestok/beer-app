import * as filters from '../../redux/selectors/filters'
import * as constants from '../fixtures/reduxActions'

describe('selector filters:', () => {
  it('should return array of three sugested beers', () => {
    expect(filters.filterSimmilarBeers(constants.duplicatedBeers, constants.particularBeer))
      .toEqual(constants.filteredNoDuplicates)
  })
})
