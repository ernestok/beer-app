import PropTypes from 'prop-types'

export const beerItemArrType = PropTypes.arrayOf(
  PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    tagline: PropTypes.string.isRequired,
    image_url: PropTypes.string,
  })
)

export const beerItemType = PropTypes.shape({
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  tagline: PropTypes.string.isRequired,
  image_url: PropTypes.string,
})

export const errorType = PropTypes.bool

export const loadingType = PropTypes.bool

export const pageType = PropTypes.number

export const beerDetailsType = PropTypes.shape({
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  tagline: PropTypes.string.isRequired,
  image_url: PropTypes.string,
  description: PropTypes.string,
  brewers_tips: PropTypes.string,
  ibu: PropTypes.number,
  abv: PropTypes.number,
})

export const clearModalBeerType = PropTypes.func

export const getBeerByIdType = PropTypes.func

export const getSimmilarAbvType = PropTypes.func

export const getSimmilarIbuType = PropTypes.func

export const historyType = PropTypes.shape({
  push: PropTypes.func
})

export const matchType = PropTypes.shape({
  params: PropTypes.shape({
    id: PropTypes.string
  })
})

export const similarBeersType = PropTypes.arrayOf(
  PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    image_url: PropTypes.string,
  })
)