import React from 'react'
import { Link } from 'react-router-dom'

class NotFoundPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      styles: null,
      lMouseX: 0,
      lMouseY: 0,
      scale: 1.1
    }

    this.onMouseMove = this.onMouseMove.bind(this)
  }

  onMouseMove(e) {
    let lMouseX = Math.min(100, e.nativeEvent.offsetX / 2 - e.clientX) / 50
    let lMouseY = Math.min(100, e.nativeEvent.offsetY / 2 - e.clientY) / 50
    let scale = 1.1 + lMouseY * (-0.001)
    this.setState({
      lMouseX,
      lMouseY,
      scale
    })
  }

  render() {
    const { lMouseX, lMouseY, scale } = this.state
    let translate = `translate(${lMouseX}px, ${lMouseY}px) scale(${scale})`

    const stylesTranslated = {
      transition: 'transform 0.8s',
      transform: translate
    }

    return (
      <div className="not-found" onMouseMove={this.onMouseMove}>
        <div className="not-found__page">
          <div className="content">
            <h1>404</h1>
            <h2>Page not found</h2>
            <p>Do not drink alone. Bring some friends</p>
            <Link to="/">Go back to Beer App</Link>
          </div>
          <img className="not-found-image" style={stylesTranslated} src="http://www.supah.it/dribbble/008/008.jpg" />
        </div>
      </div>
    )
  }
}

export default NotFoundPage