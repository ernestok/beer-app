import React from 'react'
import InfiniteScroll from "react-infinite-scroll-component"
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import * as actions from '../redux/actions/actions'
import BeerItem from './BeerItem'
import Footer from '../components/Footer'
import Intro from './Intro'
import { getNextPage, isError, isLoading, ListedBeers } from '../redux/selectors/filters'
import { beerItemArrType, errorType, loadingType, pageType } from '../types/index'

class BeerApp extends React.Component {
  state = {
    hasMore: true
  }

  componentDidMount() {
    if (this.props.beers.length < 2) {
      this.fetchBeers()
    }
  }

  fetchMoreData = () => {
    if (this.props.beers.length >= 90) {
      this.setState({ hasMore: false })
      return
    }
    setTimeout(() => {
      this.fetchBeers()
    }, 500)
  }

  fetchBeers() {
    const { getBeers, page } = this.props
    const fetchPage = page + 1

    getBeers(fetchPage)
  }

  render() {
    const { beers, isError, isLoading } = this.props

    return (
      <div className="beer__container">
        <Intro />
        {isLoading ? <div className="loader fixed"></div> : ''}
        {isError ? <div className="error">Error: something went wrong with fetching data from API</div> : ''}
        <InfiniteScroll
          className="beer__infinite-scroll"
          dataLength={beers.length}
          next={this.fetchMoreData}
          hasMore={this.state.hasMore}
          scrollThreshold={'300px'}
          loader={!isError && <div className="loader"></div>}
          endMessage={
            <span className="infinite-scroll__end-info">
              Congratulations you listed all beers.
            </span>
          }
        >
          {
            beers.map((beer) => {
              return (
                <BeerItem
                  key={beer.id}
                  beer={beer}
                />
              )
            })
          }
        </InfiniteScroll>
        {!isError && <Footer />}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    beers: ListedBeers(state),
    isError: isError(state),
    isLoading: isLoading(state),
    page: getNextPage(state)
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    getBeers: (page) => dispatch(actions.getBeers(page))
  }
}

BeerApp.propTypes = {
  beers: beerItemArrType.isRequired,
  getBeers: PropTypes.func,
  isError: errorType.isRequired,
  isLoading: loadingType.isRequired,
  page: pageType
}

export default connect(mapStateToProps, mapDispatchToProps)(BeerApp)