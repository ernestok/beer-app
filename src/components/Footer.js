import React from 'react'


const Footer = () => (
  <footer className="footer">
    <p>Best beers, crafted with love from all over the world</p>
  </footer>
)

export default Footer