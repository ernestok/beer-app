import React from 'react'
import Modal from 'react-modal'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { getBeerState, isError, isLoading, filterSimmilarBeers } from '../redux/selectors/filters'
import * as actions from '../redux/actions/actions'
import { MdClose } from 'react-icons/md'
import { beerDetailsType, clearModalBeerType, errorType, loadingType, getBeerByIdType, getSimmilarAbvType, getSimmilarIbuType, historyType, matchType, similarBeersType } from '../types/index'

Modal.setAppElement(document.getElementById('app'))

class BeerItemModal extends React.Component {
  state = {
    simmilarBeers: [],
  }

  componentDidMount() {
    const { id } = this.props.match.params
    this.mountBeer(id)
  }

  componentDidUpdate(prevProps) {
    if (this.props.beer !== prevProps.beer) {

      const simmilarBeers = this.props.simmilarBeers
      this.setState(() => {
        return {
          simmilarBeers
        }
      })
    }
  }

  componentWillUnmount() {
    this.unmountBeer()
  }

  mountBeer(id) {
    const { getBeerById, getSimmilarAbv, getSimmilarIbu } = this.props

    getBeerById(id)
    getSimmilarAbv(id)
    getSimmilarIbu(id)
  }

  unmountBeer() {
    const { clearModalBeer } = this.props
    clearModalBeer()
  }

  onRequestClose = () => {
    this.props.history.push('/')
  }

  changeBeer = (item) => {
    this.unmountBeer()
    this.mountBeer(item)
  }

  render() {
    const { beer, isError, isLoading } = this.props
    const simmBeers = this.state.simmilarBeers

    return (
      <Modal
        className="modal--box"
        isOpen={true}
        onRequestClose={this.onRequestClose}
      >
        {isLoading ? <div className="loader fixed"></div> : ''}
        {isError ? <div className="modal--wrapper error">Error: something went wrong with fetching data from API</div> : ''}
        {beer &&
          <div className="modal--wrapper">
            <MdClose
              className="modal__cancel"
              onClick={this.onRequestClose}
            />
            <div className="modal--image">
              <img src={beer.image_url} />
            </div>
            <section className="details">
              <h2 className="details__name">{beer.name}</h2>
              <div className="details__tagline">{beer.tagline}</div>
              <div className="details__scale">
                <div><span>IBU:</span> {beer.ibu}</div>
                <div><span>ABV:</span> {beer.abv}</div>
              </div>
              <div className="details__description">{beer.description}</div>
              <div className="details__tips"><span>Brewers tips: </span> {beer.brewers_tips}</div>
            </section>
            <div className="other-beers">
              <span>Check out those ones:</span>
              <div className="other-beers__wrapper">
                {simmBeers.map((item) => {
                  return (
                    <Link
                      key={item.id}
                      to={`${item.id}`}
                      onClick={() => this.changeBeer(item.id)}
                    >
                      <div className="other-beers__box" >
                        <img src={item.image_url} className="other-beers__image" />
                        <p className="other-beers__name">{item.name}</p>
                      </div>
                    </Link>
                  )
                })}
              </div>
            </div>
          </div>
        }
      </Modal>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    beer: getBeerState(state),
    isError: isError(state),
    isLoading: isLoading(state),
    simmilarBeers: filterSimmilarBeers(state.simmilarBeers, state.beer),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    clearModalBeer: () => dispatch(actions.clearModalBeer()),
    getBeerById: (id) => dispatch(actions.getBeerById(id)),
    getSimmilarAbv: (id) => dispatch(actions.getSimmilarAbv(id)),
    getSimmilarIbu: (id) => dispatch(actions.getSimmilarIbu(id))
  }
}

BeerItemModal.propTypes = {
  beer: beerDetailsType,
  clearModalBeer: clearModalBeerType,
  getBeerById: getBeerByIdType,
  getSimmilarAbv: getSimmilarAbvType,
  getSimmilarIbu: getSimmilarIbuType,
  history: historyType,
  isError: errorType.isRequired,
  isLoading: loadingType.isRequired,
  match: matchType,
  simmilarBeers: similarBeersType.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(BeerItemModal)

