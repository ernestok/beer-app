import React from 'react'

const Intro = () => (
  <div className="intro__wrapper">
    <h2 className="intro__header">
      Beer makes everything better. Drink like it's your day job.
  </h2>
    <hr></hr>
    <div className="intro__text">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna
        aliqua. Ut enim ad minim veniam, quis nostrud exercitation
        ullamco laboris nisi ut aliquip ex ea commodo consequat.
        Duis aute irure dolor in reprehenderit in voluptate velit
        esse cillum dolore eu fugiat nulla pariatur.
      </p>
    </div>
  </div>
)

export default Intro