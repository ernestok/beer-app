import React from 'react'

const Header = () => (
  <div className="header">
    <h1 className="header--title">BeerApp</h1>
  </div>
)

export default Header