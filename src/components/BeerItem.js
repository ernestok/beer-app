import React from 'react'
import { Link } from 'react-router-dom'
import { beerItemType } from '../types/index'


const BeerItem = ({ beer }) => (
  <Link to={`details/${beer.id}`}>
    <div className="beer__item">
      <div >{beer.name}</div>
      <img src={beer.image_url} className="beer-image" />
      <div >{beer.tagline}</div>
    </div>
  </Link>
)

BeerItem.propTypes = {
  beer: beerItemType.isRequired,
}

export default BeerItem