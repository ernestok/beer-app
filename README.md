## Description

Application which fetch and show beers.

## Features: ##

    1. Responsive for mobile and desktop device view.
    2. Show details of choosen beer.
    3. Suggest simmilar beers to choosen one.
    4. Infinite scroll for fetched list of beers.
    5. Code tested.
    
    
## Author information ##

Ernest Kost

ernest.kost@gmail.com



## Installation and launching ##


### 1. Installing npm packages ###

  In order to launch the app, firstly you need to install the required npm packages.
  Change your directory to folder with cloned repo then use the command:
  
  `npm install`

  
### 2. Launching the app ###

  In order to run aplication in production mode on local machine go to main folder (where the package.json is) and use command:
  
  `npm start`
  
  and open `http://localhost:8080/` in your browser
  
  
###  Used tools and frameworks ###

  
  - [ReactJS](https://reactjs.org/)
  - [Redux](https://redux.js.org/)
  - [Webpack](https://webpack.js.org)
  - [Enzyme](https://airbnb.io/enzyme/)
  - [Jest](https://jestjs.io/)
  - [SCSS](https://sass-lang.com/)
