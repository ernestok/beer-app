const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const distPath = path.join(__dirname, 'dist')

module.exports = (env, argv) => {
  return {
    mode: argv.mode === 'production' ? 'production' : 'development',
    entry: './src/app.js',
    output: {
      path: distPath,
      filename: argv.mode === 'production' ? '[name][chunkhash].js' : '[name].js'
    },
    plugins: [
      new HtmlWebpackPlugin({
        title: 'BeerApp',
        template: 'src/index.html',
        favicon: 'src/favicon.ico',
        publicPath: distPath
      }),
      new MiniCssExtractPlugin({
        filename: '[name].css',
        chunkFilename: '[id].css'
      })
    ],
    module: {
      rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.s?css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: argv.mode === 'production' ? true : false,
            },
          },
          'css-loader',
          'sass-loader'
        ]
      }]
    },
    devtool: argv.mode === 'production' ? 'none' : 'source-map',
    devServer: {
      contentBase: distPath,
      historyApiFallback: true
    }
  }
}